﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD1
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset dataset = new Dataset((@"D:\FAKS\4. semestar\RPPOON\CSV_file.csv"));
            Adapter adapter = new Adapter(new Analyzer3rdParty());
            double[] results = adapter.CalculateAveragePerColumn(dataset);
            Console.WriteLine(String.Join(" , ", results));
        }
    }
}
