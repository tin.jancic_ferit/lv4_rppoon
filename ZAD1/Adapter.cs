﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD1
{
    class Adapter : IAnalytics
    {
        private Analyzer3rdParty analyticsService;
        public Adapter(Analyzer3rdParty service)
        {
            this.analyticsService = service;
        }
        private double [][] ConvertData(Dataset dataset)
        {
            IList<List<double>> data = dataset.GetData();
            double[][] new_data = new double[data.Count][];
            int i = 0;
            foreach(List<double>value in data)
            {
                new_data[i] = new double[value.Count];
                new_data[i] = value.ToArray();
                i++;
            }
            return new_data;
        }
        public double[] CalculateAveragePerColumn(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerColumnAverage(data);
        }
        public double[] CalculateAveragePerRow(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerRowAverage(data);
        } 
    }
}
